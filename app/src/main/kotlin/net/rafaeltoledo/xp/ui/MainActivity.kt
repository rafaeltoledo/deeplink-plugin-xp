package net.rafaeltoledo.xp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import net.rafaeltoledo.xp.data.DeeplinkHandler

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    intent?.data?.let {
      DeeplinkHandler.handle(window.decorView, it)
    }
  }
}
