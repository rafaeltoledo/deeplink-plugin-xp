package net.rafaeltoledo.xp.firebase

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import net.rafaeltoledo.xp.plugin.CachedExperiments
import net.rafaeltoledo.xp.plugin.Experiment

class FirebaseCachedExperiments : CachedExperiments {

  override fun isTreated(experiment: Experiment): Boolean {
    return Firebase.remoteConfig.getBoolean(experiment.rawName())
  }
}
