package net.rafaeltoledo.xp.plugin

abstract class PluginPoint<D, T> {

  protected abstract val cachedExperiments: CachedExperiments

  fun getPlugins(dynamicDependency: D): List<T> {
    return getInternalFactories()
      .filter { cachedExperiments.isTreated(it.killSwitch) && it.isApplicable(dynamicDependency) }
      .map { it.createPlugin(dynamicDependency) }
  }

  fun getPlugin(dynamicDependency: D): T? {
    return getPlugins(dynamicDependency).firstOrNull()
  }

  protected abstract fun getInternalFactories(): List<PluginFactory<D, T>>
}
