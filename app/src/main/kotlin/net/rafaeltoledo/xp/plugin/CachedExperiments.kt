package net.rafaeltoledo.xp.plugin

interface CachedExperiments {

  fun isTreated(experiment: Experiment): Boolean
}
