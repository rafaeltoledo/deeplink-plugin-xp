package net.rafaeltoledo.xp.plugin

interface PluginFactory<D, T> {

  val killSwitch: Experiment

  fun createPlugin(dynamicDependency: D): T

  fun isApplicable(dynamicDependency: D): Boolean
}
