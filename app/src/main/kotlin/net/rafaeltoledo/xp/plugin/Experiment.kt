package net.rafaeltoledo.xp.plugin

fun interface Experiment {
  fun rawName(): String
}
