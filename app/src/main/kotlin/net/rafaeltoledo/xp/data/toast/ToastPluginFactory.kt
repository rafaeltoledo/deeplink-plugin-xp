package net.rafaeltoledo.xp.data.toast

import android.net.Uri
import net.rafaeltoledo.xp.data.DeeplinkPlugin
import net.rafaeltoledo.xp.plugin.Experiment
import net.rafaeltoledo.xp.plugin.PluginFactory

class ToastPluginFactory : PluginFactory<Uri, DeeplinkPlugin> {

  override fun createPlugin(dynamicDependency: Uri): DeeplinkPlugin {
    return ToastPlugin(dynamicDependency)
  }

  override fun isApplicable(dynamicDependency: Uri): Boolean {
    return dynamicDependency.host == "sample"
  }

  override val killSwitch = Experiment { "toast" }
}
