package net.rafaeltoledo.xp.data

import android.net.Uri
import net.rafaeltoledo.xp.data.snackbar.SnackBarPluginFactory
import net.rafaeltoledo.xp.data.toast.ToastPluginFactory
import net.rafaeltoledo.xp.plugin.CachedExperiments
import net.rafaeltoledo.xp.plugin.PluginFactory
import net.rafaeltoledo.xp.plugin.PluginPoint

class DeeplinkPluginPoint(
  override val cachedExperiments: CachedExperiments,
) : PluginPoint<Uri, DeeplinkPlugin>() {

  override fun getInternalFactories(): List<PluginFactory<Uri, DeeplinkPlugin>> {
    return listOf(
      ToastPluginFactory(),
      SnackBarPluginFactory(),
    )
  }
}
