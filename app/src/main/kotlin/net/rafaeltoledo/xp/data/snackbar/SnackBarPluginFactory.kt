package net.rafaeltoledo.xp.data.snackbar

import android.net.Uri
import net.rafaeltoledo.xp.data.DeeplinkPlugin
import net.rafaeltoledo.xp.plugin.Experiment
import net.rafaeltoledo.xp.plugin.PluginFactory

class SnackBarPluginFactory : PluginFactory<Uri, DeeplinkPlugin> {

  override fun createPlugin(dynamicDependency: Uri): DeeplinkPlugin {
    return SnackBarPlugin(dynamicDependency)
  }

  override fun isApplicable(dynamicDependency: Uri): Boolean {
    return dynamicDependency.host == "sample"
  }

  override val killSwitch = Experiment { "snackbar" }
}
