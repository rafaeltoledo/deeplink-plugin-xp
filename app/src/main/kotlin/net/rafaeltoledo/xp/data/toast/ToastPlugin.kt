package net.rafaeltoledo.xp.data.toast

import android.net.Uri
import android.view.View
import android.widget.Toast
import net.rafaeltoledo.xp.data.DeeplinkPlugin

class ToastPlugin(
  private val uri: Uri,
) : DeeplinkPlugin {

  override fun handle(view: View) {
    Toast.makeText(view.context, uri.host ?: "", Toast.LENGTH_LONG).show()
  }
}
