package net.rafaeltoledo.xp.data

import android.net.Uri
import android.view.View
import net.rafaeltoledo.xp.firebase.FirebaseCachedExperiments

object DeeplinkHandler {

  private val deeplinkPluginPoint = DeeplinkPluginPoint(FirebaseCachedExperiments())

  fun handle(view: View, uri: Uri) {
    deeplinkPluginPoint
      .getPlugin(uri)
      ?.handle(view)
  }
}
