package net.rafaeltoledo.xp.data

import android.view.View

interface DeeplinkPlugin {

  fun handle(view: View)
}
