package net.rafaeltoledo.xp.data.snackbar

import android.net.Uri
import android.view.View
import com.google.android.material.snackbar.Snackbar
import net.rafaeltoledo.xp.data.DeeplinkPlugin

class SnackBarPlugin(
  private val uri: Uri,
) : DeeplinkPlugin {

  override fun handle(view: View) {
    Snackbar.make(view, uri.host ?: "", Snackbar.LENGTH_LONG).show()
  }
}
