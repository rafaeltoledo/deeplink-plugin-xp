# Exemplo de implementação de plugins

Este repositório contém uma implementação mínima de um mecanismo de plugins controlado por feature
flags remotas. A ideia é que o código cliente não contenha lógica de controle, e somente defina
pontos de extensão e registre implementações de um plugin.

## Alguns conceitos importantes

1. Um **`Plugin`** é basicamente uma interface/protocolo que define o que um plugin deve implementar
2. Um **`PluginFactory`** é a classe que define COMO um plugin é criado e SE um plugin deve ser criado, além do mecanismo de desligar (killswitch)
3. Um **`PluginPoint`** é a criação de um ponto de extensão. É a definição de que, ali, plugins podem ser registrados para extender determinada funcionalidade.
